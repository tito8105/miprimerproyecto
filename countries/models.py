from django.db import models

class Ciudad(models.Model):
    nombre=models.CharField(db_column='NOMBRE', verbose_name='Nombre', max_length=70)
    
    class Meta:
        db_table='CIUDAD'
        verbose_name='Pais'
        verbose_name_plural='Paises'
    def __str__(self):
        return f'{self.nombre}'
    
class Departamento(models.Model):
    nombre=models.CharField(db_column='NOMBRE', verbose_name='Nombre', max_length=50)
    ciudad=models.ForeignKey(Ciudad,db_column='CIUDAD', verbose_name='Pais', on_delete=models.CASCADE)
    
    class Meta:
        db_table='DEPARTAMENTO'
        verbose_name='Departamento'
        verbose_name_plural='Departamentos'  
    def __str__(self):
        return f'{self.nombre} de {self.ciudad}'
        

class Municipalidad(models.Model):
    nombre=models.CharField(db_column=('MUNICIPALIDAD'), verbose_name='Municipalidad', max_length=50)
    departamento=models.ForeignKey(Departamento,db_column='DEPARTAMENTO', verbose_name='Municipio', on_delete=models.CASCADE)
    class Meta:
        db_table='MUNICIPALIDAD'
        verbose_name='Municipalidad'
        verbose_name_plural='Municipalidades'
    def __str__(self):
        return f'{self.nombre}, {self.departamento}'

# Create your models here.
