from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Ciudad,Departamento,Municipalidad

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display=['nombre']
    list_filter=[]
    list_editable=[]
    list_per_page=15
    search_fields=['nombre']

@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    list_display=['nombre']
    list_filter=['ciudad']
    list_editable=[]
    list_per_page=15
    search_fields=['nombre']

@admin.register(Municipalidad)
class MunicipalidadAdmin(admin.ModelAdmin):
    list_display=['nombre']
    list_filter=['departamento']
    list_editable=[]
    list_per_page=15
    search_fields=['nombre']
# Register your models here.
