from django.urls import path
from .views import DepartamentosView,MunicipalidadesView

urlpatterns = [
    path('<int:id>', DepartamentosView.as_view(),name='departamentos'),
    path('municipios/<int:id>', MunicipalidadesView.as_view(),name='municipios')
]