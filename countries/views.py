from django.shortcuts import render
from typing import Any
#from django import hhtp
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .models import Ciudad,Departamento,Municipalidad
from django.views.generic import TemplateView

class VistaInicio(TemplateView):
    template_name='home/home.html'

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get(self,request):
        data={}
        try:
            ciudades=Ciudad.objects.all()
            data['ciudades']=ciudades
            data['error']=0
        except:
            data['error']=1
        return render(request,self.template_name,data)
    
class DepartamentosView(TemplateView):
    template_name='ciudad/departamentos.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get(self,request,id=0):
        data={}
        if id!=0:
            data['coun_id']=id
            try:
                coun=Ciudad.objects.get(pk=id)
                data['coun_name']=coun.nombre
                departamentos=Departamento.objects.filter(ciudad=id)
                data['departamentos']=departamentos
                exists=len(departamentos)>0
                if exists:
                    data['error']=0
                else:
                    raise 'error'
            except:
                data['error']=1
        return render(request,self.template_name,data)

class MunicipalidadesView(TemplateView):
    template_name='departamentos/municipalidades.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get(self, request,id=0):
        data={}
        if id !=0:
            data['depa_id']=id

            try:
                depa=Departamento.objects.get(pk=id)
                data['depa_name']=depa.nombre
                municipalidades=Municipalidad.objects.filter(departamento=id)
                data['municipalidades']=municipalidades
                exists=len(municipalidades)>0
                if exists:
                    data['error']=0
                else:
                    raise 'error'
            except:    
                data['error']=1
        return render(request,self.template_name,data)
    
# Create your views hecountries/views.pyre.
